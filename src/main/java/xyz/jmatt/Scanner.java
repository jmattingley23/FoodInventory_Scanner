package xyz.jmatt;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Listens for global key-presses
 */
public class Scanner implements NativeKeyListener {
    private static StringBuilder currentBarCode;
    private boolean codeStarted = false;
    private static final ServerProxy proxy = new ServerProxy();
    private static String host;
    private static String port;

    /**
     * A key was pressed
     * @param event The key that was pressed
     */
    public void nativeKeyPressed(NativeKeyEvent event) {
        // Press escape to quit
        if(event.getKeyCode() == NativeKeyEvent.VC_ESCAPE) {
            try {
                GlobalScreen.unregisterNativeHook();
            } catch(NativeHookException error) {
                System.err.println(error);
            }
        }

        if(event.getKeyCode() == NativeKeyEvent.VC_OPEN_BRACKET) {
            currentBarCode = new StringBuilder();
            codeStarted = true;
        } else if(event.getKeyCode() == NativeKeyEvent.VC_CLOSE_BRACKET) {
            codeStarted = false;
            System.out.println("Scanned barcode: " + currentBarCode.toString());
            sendBarcodeToServer(currentBarCode.toString());
        } else if(codeStarted) {
            currentBarCode.append(NativeKeyEvent.getKeyText(event.getKeyCode()));
        }
    }

    private void sendBarcodeToServer(String barcode) {
        proxy.connect(host, port);
        proxy.processItem(barcode);
        System.out.println("Sent.");
    }

    public void nativeKeyReleased(NativeKeyEvent e) {}

    public void nativeKeyTyped(NativeKeyEvent e) {}

    public static void main(String[] args) {
        host = args[0];
        port = args[1];

        try {
            GlobalScreen.registerNativeHook();
        }
        catch (NativeHookException ex) {
            System.err.println("There was a problem registering the native hook.");
            System.err.println(ex.getMessage());

            System.exit(1);
        }
        Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
        logger.setLevel(Level.OFF);

        currentBarCode = new StringBuilder();

        GlobalScreen.addNativeKeyListener(new Scanner());
    }
}